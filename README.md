# firebase-libraries

Репозиторий для хранения библиотек Firebase для юнити.

## Хранящиеся библиотеки
```
com.google.firebase.remote-config
com.google.firebase.messaging
com.google.firebase.installations
com.google.firebase.firestore
com.google.firebase.dynamic-links
com.google.firebase.crashlytics
com.google.firebase.app
com.google.firebase.analytics
com.google.firebase.auth
com.google.firebase.functions
com.google.external-dependency-manager
com.google.android.performancetuner
```
## Подключение в юнити

1. Добавить в manifest.json этот репозиторий

```  
"scopedRegistries": [
    {
      "name": "firebase-libraries",
      "url": "https://gitlab.com/api/v4/projects/31636454/packages/npm",
      "scopes": [
        "com.google"
      ]
    }
  ]
```

2. Добавить в manifest.json необходимые библиотеки

```
    "com.google.firebase.app": "8.6.2",
    "com.google.firebase.crashlytics": "8.6.2",
    "com.google.firebase.installations": "8.6.2",
    "com.google.firebase.remote-config": "8.6.2"
```

## Заливка библиотек

Найти библиотеки можно [здесь](https://developers.google.com/unity/archive).

1. Установить npm
2. Создать Personal Access Token в настройках аккаунта
3. Создать файл .npmrc со следующим содержимым
```
registry=https://gitlab.com/api/v4/projects/31636454/packages/npm/

//gitlab.com/api/v4/projects/31636454/packages/npm/:_authToken="PERSONAL_ACCESS_TOKEN"

_auth="PERSONAL_ACCESS_TOKEN"
always-auth=true  
```
где, вместо PERSONAL_ACCESS_TOKEN подставляете созданный в пункте 2 токен

4. Распаковать .tgz архив с библиотекой и положить файл .npmrc рядом с package.json
5. Из этой папки в консоли выполнить комманду `npm publish`
